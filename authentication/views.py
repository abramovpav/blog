from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from forms import SigninForm


def signin(requst, next=""):
    if next == "":
        next = requst.GET.get('next', '/')

    if requst.user.is_authenticated():
        return redirect(next)

    context = {'next': next}
    if requst.method == 'POST':
        form = SigninForm(requst.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data["password"])
            if user is not None:
                login(requst, user)
                return redirect(next)
            else:
                context['message_error'] = 'Incorrect username or password'
    else:
        form = SigninForm()

    context['form'] = form
    return render(requst, 'apps/authentification/signin.html', context)


def signout(requst):
    if requst.user.is_authenticated():
        logout(requst)
    return redirect('home')
