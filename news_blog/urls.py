from django.conf.urls import patterns, include, url
from django.contrib import admin

from news_blog import settings

from articles.views import home_page, view_category, view_tags, search
from authentication.views import signin, signout
import profiles.urls

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^news/', include('articles.urls', namespace='articles')),

    url(r'^login', signin, name='login'),
    url(r'^logout$', signout, name='logout'),

    url(r'^users/', include(profiles.urls, namespace='users')),

    url(r'^tags$', view_tags, name='tags'),
    url(r'^search$', search, name='search'),

    url(r'^$', home_page, name='home'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    url(r'^(?P<category_slug>[A-Za-z0-9\-\_]+)$', view_category, name='view_category'),
)
