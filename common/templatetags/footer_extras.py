from django import template
from articles.models import Article, Tag, User


register = template.Library()

VISIBLE_ARTICLES_COUNT = 5
VISIBLE_TAGS_COUNT = 20
VISIBLE_USER_COUNT = 5


@register.inclusion_tag(takes_context=True, file_name='base/shared/footer_article_list.html')
def footer_latest_news(context):
    articles = Article.objects.filter(state=Article.ACTIVE)
    if not context['user'].is_authenticated():
        articles = articles.exclude(access_mask=Article.VISIBLE_REGISTERED)
    articles = articles.order_by('-pub_date')[:VISIBLE_ARTICLES_COUNT]
    return {'title': 'Latest news', 'articles': articles}


@register.inclusion_tag(file_name='base/shared/footer_tag_list.html')
def footer_top_tags():
    tags = Tag.objects.all()
    tags = sorted(tags, key=lambda x: x.articles.count(), reverse=True)
    tags = tags[:VISIBLE_TAGS_COUNT]
    return {'title': 'Tags', 'tags': tags}


@register.inclusion_tag(file_name='base/shared/footer_user_list.html')
def footer_user_tags():
    authors_with = [(x.article_set.count(), x) for x in User.objects.all()]
    authors_with = sorted(authors_with, key=lambda x: x[0], reverse=True)
    authors_with = [x for x in authors_with if x[0] > 0]
    authors = authors_with[:VISIBLE_USER_COUNT]
    return {'title': 'Top authors', 'authors': authors}