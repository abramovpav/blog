

class Action(object):
    def __init__(self, description="", date=""):
        super(Action)
        self.description = description
        self.date = date

    def __unicode__(self):
        return "Action: " + self.description + " Date:" + self.date

    def __str__(self):
        return "Action: " + self.description + " Date:" + self.date