from django.conf.urls import patterns, url
import views
from articles.views import user_articles

urlpatterns = patterns('',
    url(r'^register/$', views.create_user, name='create_user'),

    url(r'^dashboard/$', views.home_page, name='dashboard'),
    url(r'^(?P<user_name>\w+)/$', views.profile_page, name='profile_page'),

    url(r'^subscribe$', views.subscribe, name='subscribe'),
    url(r'^change_subscription$', views.change_subscr, name='change_subscr'),
    url(r'^(?P<user_name>\w+)/articles$', user_articles, name='user_articles'),
)