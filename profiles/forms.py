from django import forms
from django.contrib.auth.models import User

from .models import Profile


class RegistrationForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password')
        widgets = {
            'password': forms.PasswordInput,
        }

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.CharField(required=True)
    repeat_password = forms.CharField(required=True, widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        passwd = cleaned_data.get("password")
        repeat_passwd = cleaned_data.get("repeat_password")

        if passwd and repeat_passwd:
            if passwd != repeat_passwd:
                self._errors["repeat_password"] = self.error_class(["Passwords don't match"])
                del cleaned_data["repeat_password"]

        return cleaned_data


class EditUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', )

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    # birth_date = forms.DateField(required=False)
    # activity = forms.CharField(required=False)
    email = forms.EmailField(required=True)
    # phone_number = forms.IntegerField(required=False)
    # skype = forms.CharField(required=False)
    # profile = forms.InlineForeignKeyField(required=False)


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('birth_date', 'activity', 'phone_number', 'skype')
        # widgets = {
        #     'birth_date': forms.Input(attrs={'type': 'date'})
        # }

    birth_date = forms.DateField(required=False)
    activity = forms.CharField(required=False)
    phone_number = forms.IntegerField(required=False)
    skype = forms.CharField(required=False)
