from django.db import models
from django.contrib.auth.models import UserManager
from django.contrib.auth import get_user_model


class Profile(models.Model):
    IMAGES_UPLOAD_FOLDER = "images/user_profile_photos"

    user = models.OneToOneField(get_user_model())
    birth_date = models.DateField('birthday', blank=True, null=True)
    phone_number = models.BigIntegerField('mobile', blank=True, null=True)
    skype = models.CharField(max_length=75, blank=True, null=True)
    activity = models.CharField(max_length=75, blank=True, null=True)
    avatar = models.ImageField(upload_to=IMAGES_UPLOAD_FOLDER,
                               blank=True,
                               null=True)

    objects = UserManager()

    def __unicode__(self):
        return self.user.get_full_name()

    def sub_set(self):
        return Subscription.objects.filter(user=self.user).values_list('subscribe_to', flat=True)


class Subscription(models.Model):
    user = models.ForeignKey(get_user_model(), related_name="subscr_from")
    subscribe_to = models.ForeignKey(get_user_model(),
                                     related_name="subscr_to")