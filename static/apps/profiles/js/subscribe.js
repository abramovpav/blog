function change_subscription(from_user, to_user) {
    var csrftoken = $.cookie('csrftoken');
    $.ajax({
        url: $('#change_subscr_url').val(),
        type: 'post',
        data: {
            'csrfmiddlewaretoken': csrftoken,
            'from_user': from_user,
            'to_user': to_user
        },
        success: function(response_data) {
            if (response_data['action'] == 'added') {
                if ($('#subscr' + to_user).length) {
                    $('#subscr' + to_user).show(1200);
                }
                if ($('#follow_unsub_' + to_user).length) {
                    $('#follow_sub_' + to_user).hide(1200);
                    $('#follow_unsub_' + to_user).show(1000);
                }
                var new_value = parseInt($('#subscribed_count').text()) + 1;
                $('#subscribed_count').text(String(new_value));
            } else {
                if ($('#subscr' + to_user).length) {
                    $('#subscr' + to_user).hide(1200);
                }
                if ($('#follow_unsub_' + to_user).length) {
                    $('#follow_unsub_' + to_user).hide(1200);
                    $('#follow_sub_' + to_user).show(1000);
                }
                var new_value = parseInt($('#subscribed_count').text()) - 1;
                $('#subscribed_count').text(String(new_value));
            }
        }
    });
}

function subscribe(author_id, user_id, action) {
    var csrftoken = $.cookie('csrftoken');
    $.ajax({
        url: "/users/subscribe",
        type: "post",
        data: {
            'csrfmiddlewaretoken': csrftoken,
            'author_id': author_id,
            'user_id': user_id,
            'action': action,
            'place': 'profile_page'
        },
        success: function (response_date) {
            if (action == "subscribe") {
                $("#subscribe").hide(1200);
                $("#unsubscribe").show(1000);
            } else if (action == "unsubscribe") {
                $("#unsubscribe").hide(1200);
                $("#subscribe").show(1000);
            }
        }
    });
}