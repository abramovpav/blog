function show_comm_next_page_button(data) {
    var button = $("#comments-load");
    var comments = data['fields']['comments'];
    button.attr("onclick", "load_comments(" + comments[comments.length - 1]['id'] + ")");
    button.show();
}

function add_comments(data) {
    var comments = data['fields']['comments'];
    var index;
    for (index = 0; index < comments.length; ++index) {
        var comment = new_comment(comments[index]);
        $("#comments").find("ul").append(comment);
    }
}

function show_preloader() {
    var preloader = '<div class="preloader"> </div>';
    $("#comments-load").hide();
    $("#comments").append(preloader);
}

function hide_preloader() {
    $(".preloader").remove();
}


function load_comments(last_id) {
    show_preloader();
    var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
    var article_id = $("#article_id").val();
    //alert(article_id + " " + comments_page);
    $.ajax({
        url: "/news/comments_next_page",
        type: "post",
        data: {
            'csrfmiddlewaretoken': csrf_token,
            "article_id": article_id,
            "last_id": last_id
        },
        success: function (response_date) {
            handleResponse(response_date);
        }
    });
    hide_preloader();
}

// track scroll position
/*$(window).scroll(function() {
 var footer_top_offset = $("#comment-form-new").offset().top;
 if($(window).scrollTop() >= footer_top_offset - $(window).height()) {//$(document).height() - $(window).height()) {
 // ajax call get data from server and append to the div
 show_alert("message_alert", $(document).height(), $(window).height() + ' ' + $(window).scrollTop());
 }
 });*/

function show_edit_form(comment_id) {
    $(".comment-edit").hide();
    $(".comment-del").hide();
    var comment_content = $("#comment-content_" + comment_id);
    comment_content.hide();
    $("#comment-edit-form").remove();
    var comment_text = comment_content.text().trim();
    var edit_form = '<div id="comment-edit-form">' +
        '<textarea id="comment-edit-text" class="form-control" cols="30" rows="5">' + comment_text + '</textarea>' +

        '<input type="submit" class="btn btn-primary btn-my" id="comment-edit-submit" ' +
        'value="Update" onclick="submit_edit_comment(' + comment_id + ')"/>' +

        '<input type="submit" class="btn btn-primary btn-my" id="comment-edit-cancel" ' +
        'value="Cancel" onclick="close_edit_form(' + comment_id + ')"/>' +
        '</div>';
    comment_content.after(edit_form);
}

function submit_edit_comment(comment_id) {
    hide_alerts();
    var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
    var comment_text = $("#comment-edit-text").val();
    $.ajax({
        url: "/news/comments_update",
        type: "post",
        data: {
            'csrfmiddlewaretoken': csrf_token,
            "comment_id": comment_id,
            "comment_text": comment_text
        },
        success: function (response_date) {
            response_date['comment_text'] = comment_text;
            handleResponse(response_date);
        }
    });
}

function update_comment(data) {
    var comment_id = data['fields']['comment_id'];
    $("#comment-content_" + comment_id).text(data['comment_text']);
    close_edit_form(comment_id)
}

function close_edit_form(comment_id) {
    $("#comment-edit-form").remove();
    $("#comment-content_" + comment_id).show();
    $(".comment-edit").show();
    $(".comment-del").show();
}

function delete_comment(comment_id) {
    hide_alerts();
    var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
    $.ajax({
        url: "/news/comments_delete",
        type: "post",
        data: {
            'csrfmiddlewaretoken': csrf_token,
            "comment_id": comment_id
        },
        success: function (response_date) {
            handleResponse(response_date);
        }
    });
}

function clear_form() {
    $("#comment-new-text").val("");
}

function new_comment(data) {
    var is_user_authenticated = $('#is_user_authenticated').val();

    var result = '<li id="comment_' + data['id'] + '">' +
        '<a href="'+ data['author_url'] + '" class="comment-author">' + data['author'] + '</a>&nbsp;' +
        '<time>' + data['pub_date'] + ' in ' + data['time'] + '</time>&nbsp;' +
        '<div class="comment-action">' +
        '<a href="' + data['url'] + '" title="Permalink">#</a>&nbsp;' +
        '<span class="fa fa-pencil" title="Edit" onclick="show_edit_form(' + data['id'] + ')"></span>&nbsp;' +
        '<span class="fa fa-times" title="Delete"' + ' onclick="delete_comment(' + data['id'] + ')"></span>' +
        '</div>' +

        '<div class="action-rating comment-rating">' +
        '<span id="comment_rating_message_' + data['id'] + '" class="comment-rating-message">' +
        'You are not authorized.' +
        '<a href="/login?next=' + window.location.pathname + '">Log in</a> to rate comments</span>';

    if (data['show_rate_button']) {

        result += '<i class="fa fa-thumbs-o-up like ';

        if (data['user_liked'])
            result += 'choice';

        result += '"' +
            'onclick="set_comment_rating(' + data['id']
            + ', \'like\')"></i>&nbsp;';
    }
    result += '<span id="comment_rating_' + data['id'] + '" class="';

    if (data['user_liked'])
        result += 'top';
    else if (data['user_disliked'])
        result += 'low';

    result += '">';

    if (data['rating'] > 0)
        result += '+';

    result += data['rating'] +
        '</span>&nbsp;';

    if (data['show_rate_button']) {
        result += '<i class="fa fa-thumbs-o-down dislike ';

        if (data['user_disliked'])
            result += 'choice';

        result += '"' +
            'onclick="set_comment_rating(' + data['id'] + ', \'dislike\')"></i> ';
    }

    result += '</div>' +
        '<div class="comment-content" id="comment-content_' + data['id'] + '">' + data['text'] + '</div></li>';

    return result;
}

function create_comment(data) {
    var comment = new_comment(data['comment']);
    $("#comments ul").prepend(comment);
}

function hide_alerts() {
    $("#error_message_alert").hide();
    $("#message_alert").hide();
}

function show_alert(alert_id, header, message) {
    $("#" + alert_id).show();
    $("#" + alert_id + "_header").text(header);
    $("#" + alert_id + "_content").text(message);
    new Notification(header, {'body': message});
}

function show_error(header, message) {
    show_alert("error_message_alert", header, message);
}

function show_message(header, message) {
    //show_alert("message_alert", header, message);
}

function handleResponse(data) {
    $.each(data['actions'], function (i, action) {
        if (action == "show_error_message") {
            show_error('Error!', data['message']);
        } else if (action == "show_alert") {
            alert(data['alert_message']);
        } else if (action == "show_message") {
            show_message('Success!', data['message']);
        } else if (action == "show_new_comment") {
            create_comment(data);
            clear_form();
        } else if (action == 'delete_comment') {
            var comment_id = data['fields']['comment_id'];
            $("#comment_" + comment_id).remove();
        } else if (action == 'update_comment') {
            update_comment(data);
        } else if (action == 'close_edit_form') {
            close_edit_form(data['fields']['comment_id']);
        } else if (action == 'add_comments') {
            add_comments(data);
        } else if (action == 'show_comm_next_page_button') {
            show_comm_next_page_button(data);
        }
    });
}


$("#comment-new-submitButton").click(function () {
    hide_alerts();
    if ($("#comment-edit-form").length) {
        var message = 'You edit comment right now';
        var header = 'Error!';
        show_error(header, message);
        return;
    }
    var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
    var article_id = $('#article_id').val();
    var comment_text = $("#comment-new-text").val();
    if (comment_text == "" || comment_text == "Enter your message...") {
        message = 'Enter message, please';
        header = 'Error!';

        show_error(header, message);
    } else {
        $.ajax({
            url: "/news/comments_add",
            type: "post",
            data: {
                'csrfmiddlewaretoken': csrf_token,
                "article_id": article_id,
                "comment_text": comment_text
            },
            success: function (response_date) {
                handleResponse(response_date);
            }
        });
    }
});


// comment: rating

function set_comment_rating(comment_id, choice) {
    var csrf_token = $("input[name=csrfmiddlewaretoken]").val();

    var like_tag = $('#comment_' + comment_id + ' .like');
    var dislike_tag = $('#comment_' + comment_id + ' .dislike');
    var rating_tag = $('#comment_rating_' + comment_id);
    var current_rating = parseInt(rating_tag.html());

    if ($("#is_user_authenticated").val() == "True") {
        switch (choice) {
            case "like":
                if (dislike_tag.hasClass('choice')) {
                    dislike_tag.removeClass('choice');
                    current_rating += 1;
                } else if (!like_tag.hasClass('choice')) {
                    like_tag.addClass('choice');
                    current_rating += 1;
                }
                if (current_rating > 0)
                    current_rating = '+' + current_rating;
                rating_tag.html(current_rating);
                break;
            case "dislike":
                if (like_tag.hasClass('choice')) {
                    like_tag.removeClass('choice');
                    current_rating -= 1;
                } else if (!dislike_tag.hasClass('choice')) {
                    dislike_tag.addClass('choice');
                    current_rating -= 1;
                }
                if (current_rating > 0)
                    current_rating = '+' + current_rating;
                rating_tag.html(current_rating);
                break;
        }
    }

    $.ajax({
        url: "/news/set_comment_rating",
        type: "post",
        data: {
            'csrfmiddlewaretoken': csrf_token,
            "comment_id": comment_id,
            "choice": choice
        },
        success: function (response_date) {
            $.each(response_date['actions'], function (i, action) {
                if (action == "add_like_class") {
                    like_tag.addClass('choice');
                    rating_tag.addClass('top');
                } else if (action == "add_dislike_class") {
                    dislike_tag.addClass('choice');
                    rating_tag.addClass('low');
                } else if (action == "remove_like_class") {
                    like_tag.removeClass('choice');
                    rating_tag.removeClass('top');
                } else if (action == "remove_dislike_class") {
                    dislike_tag.removeClass('choice');
                    rating_tag.removeClass('low');
                } else if (action == "update_rating") {
                    var rating = response_date['rating'];
                    if (rating > 0)
                        rating = '+' + rating;
                    rating_tag.html(rating);
                } else if (action == "show_message") {
                    $("#comment_rating_message_" + comment_id).show();
                } else if (action == "set_is_user_authenticated_to_false") {
                    $("#is_user_authenticated").val('False');
                }
            });
        }
    });
}