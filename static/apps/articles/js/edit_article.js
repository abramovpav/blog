$(function () {

    function hide_alerts() {
        $("#error_message_alert").hide();
        $("#message_alert").hide();
    }

    function show_alert(alert_id, header, message) {
        $("#" + alert_id).show();
        $("#" + alert_id + "_header").text(header);
        $("#" + alert_id + "_content").text(message);
        new Notification(header, {'body': message});
    }

    function show_error(header, message) {
        show_alert("error_message_alert", header, message);
    }

    function show_message(header, message) {
        show_alert("message_alert", header, message);
    }

    function handleResponse(data) {
        $.each(data['actions'], function (i, action) {
            if (action == "show_error_message") {
                show_error('Error!', data['message']);
            } else if (action == "show_message") {
                show_message('Success!', data['message']);
            } else if (action == "redirect") {
                window.location.href = data['redirect_to'];
            } else if (action == "show_action_model") {
                $("#actionModelTitle").html(data['title']);
                $("#actionModelMessage").html(data['message']);
                $("#actionModel").modal('show');
            }
        });

        $.each(data['set_fields'], function (key, value) {
            if (key == "article_url") {
                var tag = $("#linkToArticle");
                tag.attr('href', value);
                tag.attr('target', "_blank");
            } else {
                $('#' + key).val(value);
            }
        });
    }

    $("#saveArticleButton").click(function () {
        hide_alerts();

        var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
        var article_id = $('#article_id').val();
        var article_title = $('#article_title').val();
        var article_description = $('#article_description').val();
        var article_category = $('#article_category').val();
        var article_important = $('#article_important').is(':checked');
        var article_access_mask = $('#article_access_mask').val();
        var article_state = $('#article_state').val();
        var article_content = $("#editor").html().toString();
        var article_tags = $('#article_tags').val();
        var article_region = $('#article_region').val();

        if (article_important == false)
            article_important = '';

        if (article_title == "" || article_description == "" || article_category == "" || article_content == "") {
            message = 'Field with * cannot be empty.';
            header = 'Error!';

            show_error(header, message);
        } else {
            $.ajax({
                url: "/news/edit/",
                type: "post",
                data: {
                    'csrfmiddlewaretoken': csrf_token,
                    "article_id": article_id,
                    "article_title": article_title,
                    "article_description": article_description,
                    "article_category": article_category,
                    "article_important": article_important,
                    "article_access_mask": article_access_mask,
                    "article_state": article_state,
                    "article_content": article_content,
                    "article_tags": article_tags,
                    "article_region": article_region
                },
                success: function (response_date) {
                    handleResponse(response_date);
                }
            });
        }
    });

    $('#article_image_form').on('submit', (function (e) {
        if ($('#article_main_image').val() == '') {
            show_error('Error!', 'You have not selected image');
        } else if ($('#article_id').val() == '') {
            show_error('Error!', 'First save article');
        } else {
            e.preventDefault();
            var formData = new FormData(this);
            formData.append('csrfmiddlewaretoken', $("input[name=csrfmiddlewaretoken]").val());

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response_date) {
                    handleResponse(response_date);
                }
            });
        }
        return false;
    }));

    $('#deleteArticleButton').click(function () {
        hide_alerts();

        var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
        var article_id = $('#article_id').val();

        if (article_id == "") {
            message = 'Article is not stored in the database';
            header = 'Error!';

            show_error(header, message);
        } else {
            $.ajax({
                url: "/news/delete",
                type: "post",
                data: {
                    'csrfmiddlewaretoken': csrf_token,
                    "article_id": article_id
                },
                success: function (response_date) {
                    handleResponse(response_date);
                }
            });
        }
        $("#deleteModel").modal('hide');
    });

    $('#redirectButton').click(function () {
        window.location.href = "/";
    });

    $('#createArticleButton').click(function () {
        window.location.href = "/news/edit";
    });
});