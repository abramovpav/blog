function subscribe(user_id, action) {
    show_preloader();
    var csrf_token = $("input[name=csrfmiddlewaretoken]").val();
    var article_id = $("#article_id").val();
    $.ajax({
        url: "/users/subscribe",
        type: "post",
        data: {
            'csrfmiddlewaretoken': csrf_token,
            'article_id': article_id,
            'user_id': user_id,
            'action': action,
            'place': 'article_page'
        },
        success: function (response_date) {
            if (action == "subscribe") {
                $("#subscribe").hide(1200);
                $("#unsubscribe").show(1000);
            } else if (action == "unsubscribe") {
                $("#unsubscribe").hide(1200);
                $("#subscribe").show(1000);
            }
        }
    });
    hide_preloader();
}