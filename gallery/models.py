from os.path import splitext, split
from tempfile import NamedTemporaryFile

from django.contrib.auth.models import User
from django.core.files import File
from django.db import models

from PIL import Image as PillowImage
from PIL.ImageOps import fit


class Image(models.Model):
    UPLOAD_FOLDER = "images"
    # Thumbnail sizes
    ADMIN_LIST_SIZE = (50, 50)
    ADMIN_SIZE = (150, 150)
    PREVIEW_SIZE = (120, 83)
    THUMBNAIL_NEWS = (80, 48)
    SIDEBAR_MAIN_IMAGE_SIZE = (80, 48)
    IMPORTANT_FIXED_MAIN_IMAGE_SIZE = (230, 130)
    LIST_ARTICLE_MAIN_IMAGE_SIZE = (120, 83)

    title = models.CharField(max_length=80, blank=True)
    image_source = models.ImageField(upload_to=UPLOAD_FOLDER)
    author = models.ForeignKey(User, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=150, blank=True)
    width = models.PositiveIntegerField(null=True, blank=True)
    height = models.PositiveIntegerField(null=True, blank=True)
    save_ratio = models.BooleanField(default=True, verbose_name="save image aspect ratio")

    admin_list_thumbnail = models.ImageField(upload_to=UPLOAD_FOLDER, null=True, blank=True, max_length=200)
    admin_thumbnail = models.ImageField(upload_to=UPLOAD_FOLDER, null=True, blank=True, max_length=200)
    preview_image = models.ImageField(upload_to=UPLOAD_FOLDER, null=True, blank=True, max_length=200)
    news_thumbnail = models.ImageField(upload_to=UPLOAD_FOLDER, null=True, blank=True, max_length=200)

    sidebar_main_image = models.ImageField(upload_to=UPLOAD_FOLDER, null=True, blank=True, max_length=200)
    important_fixed_main_image = models.ImageField(upload_to=UPLOAD_FOLDER, null=True, blank=True, max_length=200)
    list_article_main_image = models.ImageField(upload_to=UPLOAD_FOLDER, null=True, blank=True, max_length=200)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        super(Image, self).save(*args, **kwargs)

        self._generate_image(Image.SIDEBAR_MAIN_IMAGE_SIZE, '_sidebar', 'sidebar_main_image')
        self._generate_image(Image.IMPORTANT_FIXED_MAIN_IMAGE_SIZE, '_important_fixed', 'important_fixed_main_image')
        self._generate_image(Image.LIST_ARTICLE_MAIN_IMAGE_SIZE, '_list_article', 'list_article_main_image')

        # """Save image dimensions."""
        img = PillowImage.open(self.image_source.path)
        self.width, self.height = img.size

        #"""Creating admin thumbnails"""
        file_name, file_ext = splitext(self.image_source.name)

        if not self.save_ratio:
            img.thumbnail(Image.ADMIN_LIST_SIZE, PillowImage.ANTIALIAS)
        else:
            img.thumbnail((Image.ADMIN_LIST_SIZE[0], Image.ADMIN_LIST_SIZE[0] *
                           self.height / self.width),
                          PillowImage.ANTIALIAS)

        thumb_fn = file_name + "_admin_list" + file_ext
        tf = NamedTemporaryFile(delete=False)
        img.save(tf.name, "JPEG")
        self.admin_list_thumbnail.save(thumb_fn, File(open(tf.name, 'r+b')),
                                       save=False)
        tf.close()

        img = PillowImage.open(self.image_source.path)
        self.width, self.height = img.size
        if not self.save_ratio:
            img.thumbnail(Image.ADMIN_SIZE, PillowImage.ANTIALIAS)
        else:
            img.thumbnail((Image.ADMIN_SIZE[0], Image.ADMIN_SIZE[0] *
                           self.height / self.width),
                          PillowImage.ANTIALIAS)

        thumb_fn = file_name + "_admin" + file_ext
        tf = NamedTemporaryFile(delete=False)
        img.save(tf.name, "JPEG")
        self.admin_thumbnail.save(thumb_fn, File(open(tf.name, 'r+b')),
                                  save=False)
        tf.close()

        #Creating news thumbnails
        img = PillowImage.open(self.image_source.path)
        preview = fit(img, Image.PREVIEW_SIZE)

        thumb_fn = file_name + "_preview" + file_ext
        tf = NamedTemporaryFile(delete=False)
        preview.save(tf.name, "JPEG")
        self.preview_image.save(thumb_fn, File(open(tf.name, 'r+b')),
                                save=False)
        tf.close()

        img = PillowImage.open(self.image_source.path)
        thumbnail = fit(img, Image.THUMBNAIL_NEWS)

        thumb_fn = file_name + "_preview" + file_ext
        tf = NamedTemporaryFile(delete=False)
        thumbnail.save(tf.name, "JPEG")
        self.news_thumbnail.save(thumb_fn, File(open(tf.name, 'r+b')),
                                 save=False)
        tf.close()

        super(Image, self).save(*args, **kwargs)

    def size(self):
        return "{} x {}".format(self.width, self.height)

    def thumbnail_admin(self):
        return """<a href="{}"><img border="0" alt="" src="{}" />
        </a>""".format(self.image_source.url, self.admin_thumbnail.url)

    thumbnail_admin.allow_tags = True

    def thumbnail_admin_list(self):
        return """<img border="0" alt="" src="{}"
        />""".format(self.admin_list_thumbnail.url)

    thumbnail_admin_list.allow_tags = True

    def file_name(self):
        return self.image_source.name[len("images/"):]

    def _generate_image(self, size, prefix, field_name):
        file_name, file_ext = splitext(self.image_source.name)

        imgage = PillowImage.open(self.image_source.path)
        new_image = fit(imgage, size)

        new_image_filename = file_name + prefix + file_ext
        temp_file = NamedTemporaryFile(delete=False)

        new_image.save(temp_file.name, "PNG")
        self.__dict__[field_name].save(new_image_filename, File(open(temp_file.name, 'r+b')), save=False)
        temp_file.close()
