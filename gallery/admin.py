from django.contrib import admin

from .models import Image


class ImageAdmin(admin.ModelAdmin):
    search_fields = ('title', 'description', )
    list_display = ('__unicode__', 'file_name', 'author',
                    'thumbnail_admin_list', 'size', 'created', )
    list_per_page = 20
    readonly_fields = ('author', 'thumbnail_admin', )
    list_display_links = ('__unicode__', 'file_name', 'thumbnail_admin_list', )
    fieldsets = (
        ('Main attributes', {
            'fields': ('title', 'description', 'author')
        }),
        ('Image properties', {
            'fields': ('image_source', 'thumbnail_admin')
        }),
        ('Advanced options', {
            'classes': ('collapse', ),
            'description': ("Please, change this attributes "
                            "only in case if you certainly sure!"),
            'fields': ('width', 'height', 'save_ratio')
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        obj.save()

admin.site.register(Image, ImageAdmin)
#
#
# class VerboseManyToManyRawIdWidget(ManyToManyRawIdWidget):
#     """
#     A Widget for displaying ManyToMany ids in the "raw_id" interface rather than
#     in a <select multiple> box.
#     Display user-friendly value like the ForeignKeyRawId widget
#     """
#     def __init__(self, rel, attrs=None):
#         self._rel = rel
#         super(VerboseManyToManyRawIdWidget, self).__init__(rel, attrs)
#
#     def label_for_value(self, value):
#         values = value.split(',')
#         str_values = []
#         key = self.rel.get_related_field().name
#         for v in values:
#             try:
#                 obj = self.rel.to._default_manager.using(self.db).get(**{key: v})
#                 # manage unicode error
#                 x = smart_unicode(obj)
#                 # no HTML
#                 str_values += [escape(x)]
#             except self.rel.to.DoesNotExist:
#                 str_values += [u'???']
#         return u'&nbsp;<strong>%s</strong>' % (u',&nbsp;'.join(str_values))