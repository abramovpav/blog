from django.db import models
from django.contrib.auth.models import User
from django.utils.html import escape
from django.core.urlresolvers import reverse

from slugify import slugify

from gallery.models import Image


class Article(models.Model):
    ACTIVE = 'ACT'
    INACTIVE = 'INACT'
    ARCHIVE = 'ARCH'
    ARTICLE_STATE_CHOICES = (
        (ACTIVE, 'active'),
        (INACTIVE, 'inactive'),
        (ARCHIVE, 'archive'),
    )
    VISIBLE_ALL = 'VSBL_ALL'
    VISIBLE_HEADER = 'VSBL_HED'
    VISIBLE_ANNOTATION = 'VSBL_ANN'
    VISIBLE_REGISTERED = 'VSBL_REG'
    ARTICLE_ACCESS_CHOICES = (
        (VISIBLE_ALL, 'visible to all'),
        (VISIBLE_HEADER, 'visible only title'),
        (VISIBLE_ANNOTATION, 'visible only title and description'),
        (VISIBLE_REGISTERED, 'visible only for registered'),
    )

    author = models.ForeignKey(User)
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    thumbnail = models.TextField()
    full_text = models.TextField()
    creation_date = models.DateTimeField(auto_now_add=True)
    pub_date = models.DateTimeField(blank=True, null=True)
    important = models.BooleanField(default=False)
    views = models.BigIntegerField(default=0)
    main_image = models.ForeignKey(Image, blank=True, null=True, related_name="articles")
    images = models.ManyToManyField(Image, blank=True, null=True, related_name="articles_m", )
    state = models.CharField(choices=ARTICLE_STATE_CHOICES, default=INACTIVE, max_length=5)
    access_mask = models.CharField(choices=ARTICLE_ACCESS_CHOICES, default=VISIBLE_REGISTERED, max_length=8)
    tags = models.ManyToManyField('Tag', blank=True, null=True, related_name='articles')
    regions = models.ManyToManyField('Tag', blank=True, null=True, related_name='regions')
    category = models.ForeignKey('Category')
    users_liked = models.ManyToManyField(User, related_name='article_users_liked')
    users_disliked = models.ManyToManyField(User, related_name='article_users_disliked')

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'articles:full_article', (), {'article_slug': self.slug}

    def rating(self):
        return self.users_liked.count() - self.users_disliked.count()

    def comments_count(self):
        return Comment.objects.filter(article=self).count()

    def thumbnail_for_sidebar(self):
        return self.thumbnail[:68] + "..." if len(self.thumbnail) > 68 else self.thumbnail

    def thumbnail_for_tags_page(self):
        return self.thumbnail[:90] + '...' if len(self.thumbnail) >90 else self.thumbnail


class Comment(models.Model):
    article = models.ForeignKey(Article)
    author = models.ForeignKey(User)
    pub_date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    users_liked = models.ManyToManyField(User, related_name='comment_users_liked')
    users_disliked = models.ManyToManyField(User, related_name='comment_users_disliked')
    parent = models.PositiveIntegerField(blank=True, null=True)  # ?

    def __unicode__(self):
        return self.text[:20]

    def get_absolute_url(self):
        return '{}#comment_{}'.format(self.article.get_absolute_url(), self.pk)

    def as_dict(self, user=None):
        comment_as_dict = {
            'id': self.id,
            'article': self.article_id,
            'author': self.author.get_full_name(),
            'author_url': self.author.get_absolute_url(),
            'pub_date': self.pub_date.strftime('%d %B %Y'),
            'time': self.pub_date.strftime('%H:%M'),
            'text': escape(self.text),
            'rating': self.rating(),
            'parent': self.parent,
            'url': self.get_absolute_url(),
            'show_rate_button': self.author.pk != user.pk if user and user.is_authenticated() else False
        }

        if not user:
            comment_as_dict['user_liked'] = False
            comment_as_dict['user_disliked'] = False
        if user in self.users_liked.all():
            comment_as_dict['user_liked'] = True
            comment_as_dict['user_disliked'] = False
        elif user in self.users_disliked.all():
            comment_as_dict['user_liked'] = False
            comment_as_dict['user_disliked'] = True
        else:
            comment_as_dict['user_liked'] = False
            comment_as_dict['user_disliked'] = False

        return comment_as_dict

    def rating(self):
        return self.users_liked.count() - self.users_disliked.count()


class Category(models.Model):
    name = models.CharField(max_length=255)
    meta_tags = models.CharField(max_length=255, default='', blank=True, null=True)
    active = models.BooleanField(default=False)
    order = models.PositiveSmallIntegerField(unique=True)
    slug = models.SlugField()

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return 'view_category', (), {'category_slug': self.slug}

    class Meta:
        verbose_name_plural = "categories"


class Tag(models.Model):
    KEY_WORD = 'KW'
    REGION = 'RG'
    TYPE_CHOICES = (
        (KEY_WORD, 'Key Word'),
        (REGION, 'Region'),
    )
    name = models.CharField(max_length=100)
    type = models.CharField(choices=TYPE_CHOICES,
                            default=KEY_WORD,
                            max_length=2)
    slug = models.SlugField(blank=True, null=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('search') + "?tags=" + self.name

    def usage(self):
        return self.articles.count()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Tag, self).save(*args, ** kwargs)
