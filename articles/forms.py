from django import forms
from .models import Category


class SearchForm(forms.Form):
    ORDER_BY_CHOICE = (
        ('pub_date', 'By publication date'),
        ('popularity', 'By popularity'),
        ('most_view', 'By most views'),
    )
    SEARCH_IN_CHOICE = (
        ('title', 'Only title'),
        ('content', 'Only content'),
        ('everywhere', 'Everywhere'),
    )

    search = forms.CharField(max_length=200, required=False, label='Search')
    tags = forms.CharField(max_length=200, required=False, label='Tags')
    regions = forms.CharField(max_length=200, required=False, label='Regions')
    category = forms.ModelChoiceField(queryset=Category.objects.filter(active=True), empty_label="All",
                                      label='Category')
    search_in = forms.ChoiceField(required=False, choices=SEARCH_IN_CHOICE, label='Search in',
                                  widget=forms.RadioSelect(), initial='everywhere')
    order_by = forms.ChoiceField(required=False, choices=ORDER_BY_CHOICE, label='Order by')
