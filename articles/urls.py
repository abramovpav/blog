from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^edit', views.edit_article, name='edit_article'),
    url(r'^delete$', views.delete_article, name='delete_article'),
    url(r'^comments_add$', views.comment_add, name='comment_add'),
    url(r'^comments_delete$', views.comment_delete, name='comment_delete'),
    url(r'^comments_update$', views.comment_update, name='comment_update'),
    url(r'^comments_next_page$', views.comment_next_page, name='comment_next_page'),
    url(r'^set_comment_rating$', views.set_comment_rating, name='set_comment_rating'),
    url(r'^set_article_rating$', views.set_article_rating, name='set_article_rating'),

    url(r'^(?P<article_slug>[\w-]+)$', views.full_article, name='full_article'),

    url(r'^tag/(?P<tag_slug>[\w-]+)$', views.tags, name='tags'),

    #url(r'^latest/(?P<page_number>[0-9]*)', views.news, name='news'),
)