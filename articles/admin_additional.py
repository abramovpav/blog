from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import SimpleListFilter
from django.forms import ModelForm

from .models import Category


class ActiveFilter(SimpleListFilter):
    title = _('activity state')

    parameter_name = 'active'

    def lookups(self, request, model_admin):
        return (
            (None, _('active')),
            ('no', _('inactive')),
            ('all', _('all')),
        )

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        if self.value() == 'no':
            return queryset.filter(active=False)
        elif not self.value():
            return queryset.filter(active=True)


class CategoryAdminForm(ModelForm):
    class Meta:
        model = Category