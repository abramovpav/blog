__author__ = 'abramov'
from django import template

register = template.Library()


@register.filter(name='css_tag')
def add_css_tag_class(value):
    return add_css_classes(value, 'tag')


@register.filter(name='css_category')
def add_css_category_class(value):
    return add_css_classes(value, 'tag', 'category')


def add_css_classes(value, *classes):
    return "<span class='{classes}'>".format(classes=' '.join(classes)) \
           + value.__str__() + "</span>"